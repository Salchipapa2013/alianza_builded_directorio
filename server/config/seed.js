/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var User = require('../api/user/user.model');
var Directorio = require('../api/directorio/directorio.model');

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test',
    region: 'Central',
    role: 'regional',
  }, {
    provider: 'local',
    role: 'admin',
    region: 'Admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }, function() {
      console.log('finished populating users');
    }
  );
});

Directorio.find({}).remove(function() {
  Directorio.create({
    nombre:'Felipe Valencia',
    tipo:'Persona',
    codigo: '12345Ced',
    titulo:'Ingeniero',
    direccion:'Calle ABC 123',
    telefono:'123456',
    celular:'312 312 45 67',
    email:'luisfelipevalencia@laalianzacristiana.co',
    seccion:'Tecnologia',
    region:'Pacifico'   
  }, {
    nombre:'Iglesia del Perdon',
    tipo:'Iglesia',
    codigo:'101',
    titulo:'',
    direccion:'Calle Drogba 456',
    telefono:'64646464',
    celular:'99989898',
    email:'iglesia.perdon@laalianzacristiana.co',
    seccion:'',
    region:'Central',
     listado:[{ 
      nombre:'Josenfo',
      titulo:'Asesor',
      numero:'1234',
      email:'josenfo@gmail.com'
   }
   ]
  }
  , {
    nombre:'Region Central',
    tipo:'Region',
    codigo:'400',
    titulo:'',
    direccion:'Calle Region Central 456',
    telefono:'64646464',
    celular:'99989898',
    email:'region.central@laalianzacristiana.co',
    seccion:'',
    region:'Central',
     listado:[{ 
      nombre:'Josenfo',
      titulo:'Asesor',
      numero:'5678',
      email:'josenfo@gmail.com'
   }]
  }, function() {
      console.log('finished populating Directories');
    }
  );
});