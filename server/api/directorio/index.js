'use strict';

var express = require('express');
var controller = require('./directorio.controller');

var router = express.Router();

router.get('/search/:texto', controller.searchPattern);
router.get('/search/', controller.non);
router.get('/search_all/',controller.getAll);
router.get('/:id',controller.getRow);
router.post('/:id',controller.update);
router.post('/update/',controller.update);
router.delete('/:id', controller.destroy);
router.post('/', controller.create);
router.get('/hay_region/:region',controller.hayRegion);

/*router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
//router.get('/zipo',controller.zip);*/

module.exports = router;