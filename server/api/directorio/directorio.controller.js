/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /search/texto       ->  searchPattern
 */

'use strict';

var _ = require('lodash');
var Directorio = require('./directorio.model');

// Get list of Directorios
exports.searchPattern = function(req, res) {
  Directorio.aggregate([
        { $match: {
                   $text: { 
                          $search: req.params.texto 
                        } 
                  } 
        }
        //,
        // { $project: { nombre: 1, _id: 0, titulo:1, direccion:1,telefono:1,celular:1,email:1,seccion:1,region:1 } }    
    ], function (err, matches) {
        if (err) {
            console.log(err);
            return res.status(500).json({
              error: 'Cannot find any result'
            });
        }  
        return res.json(matches);       
    });

};
exports.non = function(req, res) {
   var matches=[];
   return res.json(matches); 
};

exports.getAll = function(req, res) {
  
  Directorio.aggregate([       
         { $project: { nombre: 1, _id: 1, titulo:1, direccion:1,telefono:1,celular:1,email:1,seccion:1,region:1 } }    
    ], function (err, matches) {
        if (err) {
            console.log(err);
            return res.status(500).json({
              error: 'Cannot find any result'
            });
        }  
        return res.json(matches);       
    });

};


exports.update = function(req,res) {

  var id = req.body._id;
  console.log('Update '+id);
  
  Directorio.findById(id, function (err, directorio) {
    if (err) return handleError(err);

    directorio['nombre'] = req.body.nombre;      
    directorio['direccion'] = req.body.direccion;
    directorio['telefono'] = req.body.telefono;
    directorio['celular'] = req.body.celular;
    directorio['email'] = req.body.email;   
    directorio['region'] = req.body.region;    
    directorio['names'] = req.body.names;    
   
    //if(req.body.tipo == 'Iglesia' || req.body.tipo == 'Region' ){
      directorio['codigo'] = req.body.codigo;
      directorio['listado'] = req.body.listado;        
   // }else{
     /*  directorio['titulo'] = req.body.titulo;
       directorio['seccion'] = req.body.seccion;*/
   // }
   
    directorio.save(function (err) {     
      if (err) {
        return res.status(500).json({
          'Error': 'Cannot save the directory' ,errorT:err
        });
      }
      res.json({'Response':"El registro se edito de manera exitosa!"});
    });
  });

};

exports.getRow = function (req,res){

  var id = req.params.id;
   Directorio.findById(id, function (err, directorio) {
    if (err) return handleError(err);   
    return res.json(directorio);
  });

};

exports.hayRegion = function (req,res){

  var region = req.params.region;
    Directorio.aggregate([
        { $match: {
                   'region': region,
                   'tipo': 'Region'
                  } 
        }     
    ], function (err, matches) {
        if (err) {
            console.log(err);
            return res.status(500).json({
              error: 'Cannot find any result'
            });
        }  
        return res.json(matches);       
    });

};

exports.destroy = function(req, res) {
  var directorio = req.params.id;
  Directorio.remove({_id:directorio},function(err) {
      if (err) {
      return res.status(500).json({
        'Error': 'Cannot delete the directory' ,errorT:err
      });
    }
    res.json({'Response':"El registro se elimino de manera exitosa!"});
  });
};


exports.create = function(req, res) {
  var directorio = new Directorio(req.body);

  directorio.save(function(err) {
    if (err) {
      return res.status(500).json({
        'Error': 'Cannot save the directory' ,errorT:err
      });
    }
    res.json({'Response':"El registro se agrego de manera exitosa!"});
  });
};

function handleError(res, err) {
  return res.send(500, err);
}

