'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/*
var childSchema = new Schema({



});*/
/**
 * Directorio Schema
 */
var DirectorioSchema = new Schema({
  nombre: {
    type: String,
    required: true,
    trim: true
  },
  codigo: {
    type: String,
    required: false,
    trim: true
  },
  tipo: { // Iglesia,Area,Region
    type: String,
    required: true,
    trim: true
  },
  titulo: {
    type: String,
    required: false,
    trim: true
  },
  direccion: {
    type: String,
    required: true,
    trim: true
  },
  telefono: {
    type: String,
    required: false,
    trim: true
  },
  celular: {
    type: String,
    required: false,
    trim: true
  },
  email: {
    type: String,
    required: false,
    trim: true
  },
  seccion: {
    type: String,
    required: false,
    trim: true
  },
  region: { //Mecusab Bla bla bla
    type: String,
    required: false,
    trim: true
  },
  listado: [{ 
      nombre:'string',
      titulo:'string',
      numero:'string',
      email:'string'
   }],
   names:[String]
});

var textSearch = require("mongoose-text-search");
DirectorioSchema.plugin(textSearch);
DirectorioSchema.index({
    "$**"     :"text" //This will index every field on the document that have string value     
}, {
    name: "TextIndex"
    /*weights: {
        nombre    :10,
        titulo    :2,
        direccion :5,
        telefono  :2,
        celular   :2,
        email     :6,
        seccion   :7,
        region    :8 
    }*/
});

/*DirectorioSchema.index({
    nombre     :"text",
    titulo     :"text",
    direccion  :"text", 
    telefono   :"text",
    celular    :"text",
    email      :"text",
    seccion    :"text",
    region     :"text"
}, {
    name: "search_pattern_index",
    weights: {
        nombre    :10,
        titulo    :2,
        direccion :5,
        telefono  :2,
        celular   :2,
        email     :6,
        seccion   :7,
        region    :8 
    }
});*/




/**
 * Validations
 */
DirectorioSchema.path('nombre').validate(function(nombre) {
  return !!nombre;
}, 'Nombre cannot be blank');
DirectorioSchema.path('direccion').validate(function(direccion) {
  return !!direccion;
}, 'Direccion cannot be blank');
DirectorioSchema.path('telefono').validate(function(telefono) {
  return !!telefono;
}, 'Telefono cannot be blank');
DirectorioSchema.path('celular').validate(function(celular) {
  return !!celular;
}, 'Celular cannot be blank');
DirectorioSchema.path('email').validate(function(email) {
  return !!email;
}, 'Email cannot be blank');
DirectorioSchema.path('tipo').validate(function(tipo) {
  return !!tipo;
}, 'Tipo es Obligatorio');

module.exports = mongoose.model('Directorio', DirectorioSchema);